/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./apps/dummy-data-app/src/app/csv.ts":
/***/ ((__unused_webpack_module, exports, __webpack_require__) => {


Object.defineProperty(exports, "__esModule", ({ value: true }));
exports.getGeneratedData = exports.getDummyData = exports.getDefaultCSV = exports.readCSV = exports.createCSV = void 0;
const tslib_1 = __webpack_require__("tslib");
const fs = tslib_1.__importStar(__webpack_require__("fs"));
const csvStringify = tslib_1.__importStar(__webpack_require__("csv-stringify"));
const stringify = csvStringify.stringify;
const path = tslib_1.__importStar(__webpack_require__("path"));
const faker_1 = __webpack_require__("@faker-js/faker");
const csvParse = tslib_1.__importStar(__webpack_require__("csv-parse"));
const parse = csvParse.parse;
const createCSV = (req, res) => {
    const data = req.body.data;
    if (!data || !data.length) {
        return res
            .status(400)
            .json({ success: false, message: 'Please enter at least 1 row' });
    }
    stringify(data, {
        header: true,
    }, function (err, str) {
        const pathFiles = `${path.join(__dirname, './files/')}default.csv`;
        //create the files directory if it doesn't exist
        if (!fs.existsSync(path.join(__dirname, './files'))) {
            fs.mkdirSync(path.join(__dirname, './files'));
        }
        fs.writeFile(pathFiles, str, function (err) {
            if (err) {
                console.error(err);
                return res
                    .status(400)
                    .json({ success: false, message: 'An error occurred' });
            }
            res.download(pathFiles, 'default.csv');
        });
    });
};
exports.createCSV = createCSV;
const readCSV = (req, res) => {
    const file = req.file;
    const data = fs.readFileSync(file.path);
    parse(data, (err, records) => {
        if (err) {
            console.error(err);
            return res
                .status(400)
                .json({ success: false, message: 'An error occurred' });
        }
        return res.json({ result: records });
    });
};
exports.readCSV = readCSV;
const getDefaultCSV = (req, res) => {
    const data = fs.readFileSync(path.join(__dirname, './assets/default.csv'));
    parse(data, (err, records) => {
        if (err) {
            console.error(err);
            return res
                .status(400)
                .json({ success: false, message: 'An error occurred' });
        }
        return res.json({ result: records });
    });
};
exports.getDefaultCSV = getDefaultCSV;
const getDummyData = (req, res) => {
    const data = fs.readFileSync(path.join(__dirname, './files/default.csv'));
    parse(data, (err, records) => {
        if (err) {
            return res
                .status(400)
                .json({ success: false, message: 'An error occurred' });
        }
        let skip = Number.parseInt(req.query.skip);
        let take = Number.parseInt(req.query.take);
        let filter = req.query.filter;
        const sortValue = req.query.sort;
        const groupValue = req.query.group;
        if (isNaN(skip) || skip === undefined || skip === null) {
            skip = 0;
        }
        if (isNaN(take) || take === undefined || take === null) {
            take = records.length - 1;
        }
        else {
            skip = skip * take;
        }
        const mapper = (item) => {
            const keys = records[0];
            const obj = {};
            keys.forEach((key, index) => {
                obj[key] = item[index];
            });
            return obj;
        };
        if (filter) {
            try {
                filter = JSON.parse(filter);
                const conditions = buildConditions(filter);
                let result = records.slice(1);
                result = result.filter((item) => {
                    const keys = records[0];
                    let currentConditions = conditions;
                    keys.forEach((key, index) => {
                        currentConditions = currentConditions
                            .split(`${key}_key`)
                            .join(`"${item[index].toString()}"`);
                    });
                    const checkFn = Function(`return ${currentConditions}`);
                    return checkFn();
                });
                result = result.map(mapper);
                result = sort(result, sortValue, groupValue);
                result = result.slice(skip, skip + take);
                return res.json({ result: result, totalCount: result.length });
            }
            catch (error) {
                return res.status(400).json({ success: false, message: error });
            }
        }
        else {
            let result = records.slice(1).map(mapper);
            result = sort(result, sortValue, groupValue);
            result = result.slice(skip, skip + take);
            return res.json({ result: result, totalCount: result.length });
        }
    });
};
exports.getDummyData = getDummyData;
// eslint-disable-next-line @typescript-eslint/no-explicit-any
const buildConditions = (conditions) => {
    if (conditions.length === 3 &&
        !Array.isArray(conditions[0]) &&
        !Array.isArray(conditions[2])) {
        return buildCondition(conditions);
    }
    let str = '';
    for (let i = 0; i < conditions.length; i++) {
        if (Array.isArray(conditions[i])) {
            str = str + buildConditions(conditions[i]);
        }
        else if (conditions[i] === 'and') {
            str = str + ' && ';
        }
        else if (conditions[i] === 'or') {
            str = str + ' || ';
        }
        else {
            str = str + conditions[i] + ')';
        }
    }
    return `(${str})`;
};
const buildCondition = (condition) => {
    const operator = getOperator(condition[1]);
    switch (operator) {
        case 'contains':
            return `${condition[0]}_key.toString().toLowerCase().includes("${condition[2]
                .toString()
                .toLowerCase()}")`;
        case 'notcontains':
            return `!${condition[0]}_key.toString().toLowerCase().includes("${condition[2]
                .toString()
                .toLowerCase()}")`;
        case 'startswith':
            return `${condition[0]}_key.toString().toLowerCase().startsWith("${condition[2]
                .toString()
                .toLowerCase()}")`;
        case 'endswith':
            return `${condition[0]}_key.toString().toLowerCase().endsWith("${condition[2]
                .toString()
                .toLowerCase()}")`;
        default:
            return `${condition[0]}_key ${getOperator(condition[1])} "${condition[2].toString()}"`;
    }
};
const getOperator = (operator) => {
    if (operator == '=') {
        return '==';
    }
    return operator;
};
const createIdentity = () => {
    return {
        id: faker_1.faker.datatype.uuid(),
        first_name: faker_1.faker.name.firstName(),
        last_name: faker_1.faker.name.lastName(),
        email: faker_1.faker.internet.email(),
        gender: faker_1.faker.name.gender(),
        ip_address: faker_1.faker.internet.ip(),
    };
};
const createIdentities = (row) => {
    const data = [];
    for (let i = 0; i < row; i++) {
        data.push(createIdentity());
    }
    return data;
};
const getGeneratedData = (req, res) => {
    const rowNumber = Number.parseInt(req.query.rows);
    if (rowNumber) {
        const data = createIdentities(rowNumber);
        return res.json({ result: data, totalCount: data.length });
    }
    return res.json({ result: [], totalCount: 0 });
};
exports.getGeneratedData = getGeneratedData;
const sort = (data, sortValue, groupValue) => {
    if (!data || (!sortValue && !groupValue))
        return data;
    sortValue = sortValue === undefined ? [] : JSON.parse(sortValue);
    groupValue = groupValue === undefined ? null : JSON.parse(groupValue);
    try {
        if (groupValue) {
            if (typeof groupValue === 'string') {
                sortValue === null || sortValue === void 0 ? void 0 : sortValue.unshift({
                    selector: groupValue,
                    desc: false,
                });
            }
            else if (typeof groupValue === 'object') {
                sortValue.unshift(groupValue);
            }
        }
        const result = data === null || data === void 0 ? void 0 : data.sort((a, b) => {
            if (sortValue) {
                for (let i = 0; i < sortValue.length; i++) {
                    let first = b[sortValue[i].selector]
                        .toString()
                        .localeCompare(a[sortValue[i].selector].toString());
                    let second = a[sortValue[i].selector]
                        .toString()
                        .localeCompare(b[sortValue[i].selector].toString());
                    const isNumber = checkNumber(data, sortValue[i].selector);
                    if (isNumber) {
                        first =
                            parseFloat(b[sortValue[i].selector]) -
                                parseFloat(a[sortValue[i].selector]);
                        second =
                            parseFloat(a[sortValue[i].selector]) -
                                parseFloat(b[sortValue[i].selector]);
                    }
                    if (sortValue[i].desc) {
                        if (first) {
                            return first;
                        }
                    }
                    if (!sortValue[i].desc) {
                        if (second) {
                            return second;
                        }
                    }
                }
            }
        });
        return result;
    }
    catch (error) {
        return data;
    }
};
const checkNumber = (array, selector) => {
    return array === null || array === void 0 ? void 0 : array.every((element) => {
        if (typeof element[`${selector}`] != 'string')
            return false;
        return (!isNaN(element[`${selector}`]) &&
            !isNaN(parseFloat(element[`${selector}`])));
    });
};


/***/ }),

/***/ "./apps/dummy-data-app/src/index.ts":
/***/ ((module, exports, __webpack_require__) => {


/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */
Object.defineProperty(exports, "__esModule", ({ value: true }));
const tslib_1 = __webpack_require__("tslib");
const express_1 = tslib_1.__importDefault(__webpack_require__("express"));
const bodyParser = tslib_1.__importStar(__webpack_require__("body-parser"));
const path = tslib_1.__importStar(__webpack_require__("path"));
const os = tslib_1.__importStar(__webpack_require__("os"));
const multer_1 = tslib_1.__importDefault(__webpack_require__("multer"));
const csv_1 = __webpack_require__("./apps/dummy-data-app/src/app/csv.ts");
const cors_1 = tslib_1.__importDefault(__webpack_require__("cors"));
const upload = (0, multer_1.default)({ dest: os.tmpdir() });
const app = (0, express_1.default)();
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(express_1.default.static(path.join(__dirname, './public')));
app.use((0, cors_1.default)({
    origin: "*",
    methods: ['GET', 'POST', 'DELETE', 'UPDATE', 'PUT', 'PATCH']
}));
app.get('/api', (req, res) => {
    res.send({ message: 'Welcome to express-app!' });
});
app.post('/api/create', csv_1.createCSV);
app.post('/api/read', upload.single('file'), csv_1.readCSV);
app.get('/api/get-default', csv_1.getDefaultCSV);
app.get('/api/get-dummy', csv_1.getDummyData);
app.get('/api/generate', csv_1.getGeneratedData);
const port = process.env.PORT || 3000;
const server = app.listen(port, () => {
    console.log(`Listening at http://localhost:${port}/api`);
});
server.on('error', console.error);
module.exports = app;


/***/ }),

/***/ "@faker-js/faker":
/***/ ((module) => {

module.exports = require("@faker-js/faker");

/***/ }),

/***/ "body-parser":
/***/ ((module) => {

module.exports = require("body-parser");

/***/ }),

/***/ "cors":
/***/ ((module) => {

module.exports = require("cors");

/***/ }),

/***/ "csv-parse":
/***/ ((module) => {

module.exports = require("csv-parse");

/***/ }),

/***/ "csv-stringify":
/***/ ((module) => {

module.exports = require("csv-stringify");

/***/ }),

/***/ "express":
/***/ ((module) => {

module.exports = require("express");

/***/ }),

/***/ "multer":
/***/ ((module) => {

module.exports = require("multer");

/***/ }),

/***/ "tslib":
/***/ ((module) => {

module.exports = require("tslib");

/***/ }),

/***/ "fs":
/***/ ((module) => {

module.exports = require("fs");

/***/ }),

/***/ "os":
/***/ ((module) => {

module.exports = require("os");

/***/ }),

/***/ "path":
/***/ ((module) => {

module.exports = require("path");

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	
/******/ 	// startup
/******/ 	// Load entry module and return exports
/******/ 	// This entry module is referenced by other modules so it can't be inlined
/******/ 	var __webpack_exports__ = __webpack_require__("./apps/dummy-data-app/src/index.ts");
/******/ 	var __webpack_export_target__ = exports;
/******/ 	for(var i in __webpack_exports__) __webpack_export_target__[i] = __webpack_exports__[i];
/******/ 	if(__webpack_exports__.__esModule) Object.defineProperty(__webpack_export_target__, "__esModule", { value: true });
/******/ 	
/******/ })()
;
//# sourceMappingURL=index.js.map