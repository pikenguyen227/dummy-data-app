import * as fs from 'fs';
import * as csvStringify from 'csv-stringify';
const stringify = csvStringify.stringify;
import * as path from 'path';

import { faker } from '@faker-js/faker';

import * as csvParse from 'csv-parse';
const parse = csvParse.parse;

const createCSV = (req, res) => {
  const data = req.body.data;

  if (!data || !data.length) {
    return res
      .status(400)
      .json({ success: false, message: 'Please enter at least 1 row' });
  }

  stringify(
    data,
    {
      header: true,
    },
    function (err, str) {
      const pathFiles = `${path.join(__dirname, './files/')}default.csv`;
      //create the files directory if it doesn't exist
      if (!fs.existsSync(path.join(__dirname, './files'))) {
        fs.mkdirSync(path.join(__dirname, './files'));
      }
      fs.writeFile(pathFiles, str, function (err) {
        if (err) {
          console.error(err);
          return res
            .status(400)
            .json({ success: false, message: 'An error occurred' });
        }

        res.download(pathFiles, 'default.csv');
      });
    }
  );
};

const readCSV = (req, res) => {
  const file = (req as any).file;

  const data = fs.readFileSync(file.path);
  parse(data, (err, records) => {
    if (err) {
      console.error(err);
      return res
        .status(400)
        .json({ success: false, message: 'An error occurred' });
    }

    return res.json({ result: records });
  });
};

const getDefaultCSV = (req, res) => {
  const data = fs.readFileSync(path.join(__dirname, './assets/default.csv'));
  parse(data, (err, records) => {
    if (err) {
      console.error(err);
      return res
        .status(400)
        .json({ success: false, message: 'An error occurred' });
    }

    return res.json({ result: records });
  });
};

const getDummyData = (req, res) => {
  const data = fs.readFileSync(path.join(__dirname, './files/default.csv'));
  parse(data, (err, records) => {
    if (err) {
      return res
        .status(400)
        .json({ success: false, message: 'An error occurred' });
    }
    let skip = Number.parseInt(req.query.skip);
    let take = Number.parseInt(req.query.take);
    let filter = req.query.filter;
    const sortValue = req.query.sort;
    const groupValue = req.query.group;
    if (isNaN(skip) || skip === undefined || skip === null) {
      skip = 0;
    }
    if (isNaN(take) || take === undefined || take === null) {
      take = records.length - 1;
    } else {
      skip = skip * take;
    }
    const mapper = (item) => {
      const keys = records[0];
      const obj = {};
      keys.forEach((key, index) => {
        obj[key] = item[index];
      });
      return obj;
    };

    if (filter) {
      try {
        filter = JSON.parse(filter);
        const conditions = buildConditions(filter);
        let result = records.slice(1);

        result = result.filter((item) => {
          const keys = records[0];
          let currentConditions = conditions;
          keys.forEach((key, index) => {
            currentConditions = currentConditions
              .split(`${key}_key`)
              .join(`"${item[index].toString()}"`);
          });
          const checkFn = Function(`return ${currentConditions}`);
          return checkFn();
        });
        result = result.map(mapper);
        result = sort(result, sortValue, groupValue);
        result = result.slice(skip, skip + take);
        return res.json({ result: result, totalCount: result.length });
      } catch (error) {
        return res.status(400).json({ success: false, message: error });
      }
    } else {
      let result = records.slice(1).map(mapper);
      result = sort(result, sortValue, groupValue);
      result = result.slice(skip, skip + take);
      
      return res.json({ result: result, totalCount: result.length });
    }
  });
};

// eslint-disable-next-line @typescript-eslint/no-explicit-any
const buildConditions = (conditions: any) => {
  if (
    conditions.length === 3 &&
    !Array.isArray(conditions[0]) &&
    !Array.isArray(conditions[2])
  ) {
    return buildCondition(conditions);
  }
  let str = '';
  for (let i = 0; i < conditions.length; i++) {
    if (Array.isArray(conditions[i])) {
      str = str + buildConditions(conditions[i]);
    } else if (conditions[i] === 'and') {
      str = str + ' && ';
    } else if (conditions[i] === 'or') {
      str = str + ' || ';
    } else {
      str = str + conditions[i] + ')';
    }
  }
  return `(${str})`;
};

const buildCondition = (condition: any) => {
  const operator = getOperator(condition[1]);
  switch (operator) {
    case 'contains':
      return `${
        condition[0]
      }_key.toString().toLowerCase().includes("${condition[2]
        .toString()
        .toLowerCase()}")`;
    case 'notcontains':
      return `!${
        condition[0]
      }_key.toString().toLowerCase().includes("${condition[2]
        .toString()
        .toLowerCase()}")`;
    case 'startswith':
      return `${
        condition[0]
      }_key.toString().toLowerCase().startsWith("${condition[2]
        .toString()
        .toLowerCase()}")`;
    case 'endswith':
      return `${
        condition[0]
      }_key.toString().toLowerCase().endsWith("${condition[2]
        .toString()
        .toLowerCase()}")`;
    default:
      return `${condition[0]}_key ${getOperator(
        condition[1]
      )} "${condition[2].toString()}"`;
  }
};

const getOperator = (operator: string) => {
  if (operator == '=') {
    return '==';
  }
  return operator;
};

const createIdentity = () => {
  return {
    id: faker.datatype.uuid(),
    first_name: faker.name.firstName(),
    last_name: faker.name.lastName(),
    email: faker.internet.email(),
    gender: faker.name.gender(),
    ip_address: faker.internet.ip(),
  };
};

const createIdentities = (row: number) => {
  const data = [];
  for (let i = 0; i < row; i++) {
    data.push(createIdentity());
  }
  return data;
};

const getGeneratedData = (req, res) => {
  const rowNumber = Number.parseInt(req.query.rows);
  if (rowNumber) {
    const data = createIdentities(rowNumber);
    return res.json({ result: data, totalCount: data.length });
  }
  return res.json({ result: [], totalCount: 0 });
};

const sort = (data, sortValue, groupValue) => {
  if (!data || (!sortValue && !groupValue)) return data;
  sortValue = sortValue === undefined ? [] : JSON.parse(sortValue);
  groupValue = groupValue === undefined ? null : JSON.parse(groupValue);
  try {
    if (groupValue) {
      if (typeof groupValue === 'string') {
        sortValue?.unshift({
          selector: groupValue,
          desc: false,
        });
      } else if (typeof groupValue === 'object') {
        sortValue.unshift(groupValue);
      }
    }
    const result = data?.sort((a: any, b: any) => {
      if (sortValue) {
        for (let i = 0; i < sortValue.length; i++) {
          let first = b[sortValue[i].selector]
            .toString()
            .localeCompare(a[sortValue[i].selector].toString());
          let second = a[sortValue[i].selector]
            .toString()
            .localeCompare(b[sortValue[i].selector].toString());
          const isNumber = checkNumber(data, sortValue[i].selector);
          if (isNumber) {
            first =
              parseFloat(b[sortValue[i].selector]) -
              parseFloat(a[sortValue[i].selector]);
            second =
              parseFloat(a[sortValue[i].selector]) -
              parseFloat(b[sortValue[i].selector]);
          }
          if (sortValue[i].desc) {
            if (first) {
              return first;
            }
          }
          if (!sortValue[i].desc) {
            if (second) {
              return second;
            }
          }
        }
      }
    });

    return result;
  } catch (error) {
    return data;
  }
};

const checkNumber = (array, selector) => {
  return array?.every((element: any) => {
    if (typeof element[`${selector}`] != 'string') return false;
    return (
      !isNaN(element[`${selector}`]) &&
      !isNaN(parseFloat(element[`${selector}`]))
    );
  });
};

export { createCSV, readCSV, getDefaultCSV, getDummyData, getGeneratedData };
