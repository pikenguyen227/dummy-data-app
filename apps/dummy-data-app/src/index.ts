/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

 import express from 'express';
 import * as bodyParser from 'body-parser';
 import * as path from 'path';
 import * as os from 'os';
 import multer from 'multer';
 import { createCSV, getDefaultCSV, readCSV, getDummyData, getGeneratedData } from './app/csv';
 import cors from "cors";
 const upload = multer({ dest: os.tmpdir() })
 
 
 const app = express();

 
 app.use(bodyParser.json({limit: '50mb'}));
 app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
 app.use(express.static(path.join(__dirname, './public')));
 app.use(cors({
   origin: "*",
   methods: ['GET','POST','DELETE','UPDATE','PUT','PATCH']
 }));
 
 app.get('/api', (req, res) => {
   res.send({ message: 'Welcome to express-app!' });
 });
 
 app.post('/api/create', createCSV);
 
 app.post('/api/read', upload.single('file'), readCSV);
 
 app.get('/api/get-default', getDefaultCSV);
 
 app.get('/api/get-dummy', getDummyData);
 
 app.get('/api/generate', getGeneratedData )
 
 const port = process.env.PORT || 3000;
 const server = app.listen(port, () => {
   console.log(`Listening at http://localhost:${port}/api`);
 });
 server.on('error', console.error);
 
 module.exports = app;